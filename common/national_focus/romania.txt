focus_tree = {
	id = romanian_focus

	country = {
		factor = 0

		modifier = {
			add = 10
			tag = ROM
		}
	}

	default = no

	focus = {
        id = democracy_first
        icon = GFX_goal_generic_demand_territory
        x = 2
        y = 0

		cost = 10

        ai_will_do = {
            factor = 12
        }

		completion_reward = {
			add_ideas = liberty_ethos_focus
            add_ideas = legionary_anger
		}

		mutually_exclusive = { focus = absolutist_coup }
	}

	focus = {
        id = absolutist_coup
        icon = GFX_goal_generic_neutrality_focus
        x = 4
        y = 0

		cost = 10

        ai_will_do = {
            factor = 12
        }

		complete_tooltip = {
			custom_effect_tooltip = ROM_absolutist_coup_complete
		}

		completion_reward = {
			add_political_power = 120
			add_ideas = nationalism
			add_ideas = collectivist_ethos_focus
			country_event = { id = romania.7 }
		}

		mutually_exclusive = { focus = democracy_first }
	}

	focus = {
	    id = lift_commie_ban
	    icon = GFX_goal_support_communism
	    x = 0
	    y = 1
        ai_will_do = {
            factor = 12
        }

		cost = 10

        completion_reward = {
            remove_ideas = liberty_ethos_focus
			add_ideas = communist_partisans_recruiting
        }

		bypass = {
			AND = {
				has_government = communism
				NOT = {
					has_idea = liberty_ethos_focus
				}
			}
		}

        prerequisite = { focus = democracy_first }

        mutually_exclusive = { focus = ban_extremism }
	}

	focus = {
		id = befriend_ussr

		icon = GFX_goal_generic_major_alliance

		cost = 10

		available = {
			has_government = communism
		}

	    x = 0
	    y = 2
        ai_will_do = {
            factor = 12
        }

		completion_reward = {
            add_ideas = internationalism
			SOV = {
				add_opinion_modifier = { target = ROM modifier = befriended_by_country }
			}
		}

		prerequisite = { focus = lift_commie_ban }
	}

	focus = {
		id = ask_ussr_for_industrial_help
		icon = GFX_goal_generic_construct_civ_factory

		cost = 10

	    x = 0
	    y = 3
        ai_will_do = {
            factor = 12
        }

		completion_reward = {
			add_political_power = 120
			SOV = {
				country_event = romania.8
			}
		}

		prerequisite = { focus = befriend_ussr }
	}

	focus = {
	    id = ban_extremism
	    icon = GFX_goal_support_democracy
	    x = 2
	    y = 1

		cost = 10

        ai_will_do = {
            factor = 12
        }

        completion_reward = {
			add_ideas = extremism_banned
        }

        prerequisite = { focus = democracy_first }

        mutually_exclusive = { focus = lift_commie_ban }
	}

	focus = {
		id = ROM_why_we_fight_focus
		icon = GFX_goal_generic_propaganda
	    x = 2
	    y = 2

		available = {
			OR = {
				threat > 0.75
				has_defensive_war = yes
			}
		}

		cost = 10

        ai_will_do = {
            factor = 12
        }

        completion_reward = {
			set_rule = { can_create_factions = yes }
			add_ideas = why_we_fight_focus
        }

        prerequisite = { focus = ban_extremism }
	}

	focus = {
	    id = support_royalists
	    icon = GFX_goal_generic_political_pressure
	    x = 4
	    y = 1

		cost = 10

        ai_will_do = {
            factor = 12
        }

        completion_reward = {
			add_political_power = 120
            remove_ideas = liberty_ethos_focus
			set_politics = {
				ruling_party = neutrality
			}
        }

        prerequisite = { focus = absolutist_coup }

        mutually_exclusive = { focus = support_legionnaires }
	}

	focus = {
	    id = support_legionnaires
	    icon = GFX_goal_support_fascism
	    x = 6
	    y = 1

		cost = 10

        ai_will_do = {
            factor = 12
        }

        completion_reward = {
			set_politics = {
				ruling_party = fascism
			}
        }

        prerequisite = { focus = absolutist_coup }

        mutually_exclusive = { focus = support_royalists }
	}

	focus = {
	    id = kill_codreanu
	    icon = GFX_goal_generic_CAS
	    x = 4
	    y = 2

		cost = 10

        ai_will_do = {
            factor = 12
        }

        completion_reward = {
			add_political_power = 120
			add_ideas = legionary_anger
        }

        prerequisite = { focus = support_royalists }
	}

	focus = {
	    id = crackdown_extremists
	    icon = GFX_goal_generic_defence
	    x = 3
	    y = 3

		cost = 20

        ai_will_do = {
            factor = 12
        }

        completion_reward = {
			add_ideas = fascism_defeated
			add_ideas = communism_defeated
			add_ideas = extremism_destroyed
			remove_ideas = extremism_banned
			remove_ideas = legionary_anger
        }

        prerequisite = { focus = kill_codreanu focus = ban_extremism }
	}
	focus = {
		id = ROM_indoctrination_focus
		icon = GFX_goal_generic_propaganda
	    x = 0
	    y = 4

		cost = 10

        ai_will_do = {
            factor = 12
        }

        completion_reward = {
			add_ideas = indoctrination_focus
			add_political_power = 150
        }

        prerequisite = { focus = ask_ussr_for_industrial_help }
	}

	focus = {
		id = ROM_join_allies
		icon = GFX_goal_anschluss
		available = {
			is_puppet = no
			has_government = democratic
			ENG = {
				exists = yes
				has_government = democratic
			}
			NOT = { has_war_with = ENG }
		}
		bypass = {
			is_in_faction_with = ENG
		}
	 	prerequisite = { focus = crackdown_extremists }
		x = 3
		y = 4
		cost = 10
		ai_will_do = {
			factor = 0.9

			modifier = {
				factor = 2
				ai_irrationality < 15
			}
			modifier = {
				factor = 2
				ai_irrationality < 20
			}
			modifier = {
				factor = 0.5
				ai_irrationality > 30
			}
		}
		completion_reward = {
			ENG = { country_event = { id = romania.9 } }
		}
	}
}
